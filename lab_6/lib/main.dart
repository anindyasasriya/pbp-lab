import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Welcome to Flutter',
      home: new Halaman(),
    );
  }
}

/// This is the stateless widget that the main application instantiates.
class Halaman extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      drawer: Drawer(
        child: Text('Yo!'),
      ),
      appBar: AppBar(
        backgroundColor: Colors.brown.shade50,
        title: const Text('PBP-09'),
      ),
      body: ListView(
        padding: EdgeInsets.all(24),
        children: [
          Text(
            'Data Covid-19 Nasional',
            style: TextStyle(
              fontSize: 32,
              fontWeight: FontWeight.bold,
            ),
          ),
          const SizedBox(height: 8),
          Text(
              'Data berikut merupakan akumulasi data pasien Covid-19 yang dirawat di Indonesia'),
          cardSatu(),
          cardDua(),
          cardTiga(),
        ],
      ),
    );
  }

  Widget cardSatu() => Card(
        child: Column(
          children: [
            const SizedBox(height: 13),
            Text(
              'Kasus Positif Covid-19 Nasional',
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
            Text(
              '4.247.320',
              style: TextStyle(fontWeight: FontWeight.bold, color: Colors.red),
            ),
          ],
        ),
      );

  Widget cardDua() => Card(
        child: Column(
          children: [
            const SizedBox(height: 13),
            Text(
              'Kasus Positif Covid-19 Nasional',
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
            Text(
              '4.247.320',
              style: TextStyle(fontWeight: FontWeight.bold, color: Colors.red),
            ),
          ],
        ),
      );

  Widget cardTiga() => Card(
        child: Column(
          children: [
            const SizedBox(height: 13),
            Text(
              'Kasus Positif Covid-19 Nasional',
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
            Text(
              '4.247.320',
              style: TextStyle(fontWeight: FontWeight.bold, color: Colors.red),
            ),
          ],
        ),
      );
}

