1. Apakah perbedaan antara JSON dan XML?
JSON (JavaScript Object Notation) adalah format pemindahan data yang ringan dan merupakan bahasa independen. JSON adalah bahasa yang didasarkan pada bahasa JavaScript sehingga mudah untuk dipahami dan dibuat. JSON  didesain menjadi self-describing agar lebih mudah dimengerti. Sintaks dari JSON sendiri merupakan turunan dari Object JavaScript, namun format JSON berbentuk text, sehingga kode untuk JSON banyak terdapat di bahasa pemrograman. Data pada JSON disimpan dalam bentuk key (kunci) dan value (nilai)

XML (Extensible Markup Language) dibuat khusus untuk membawa data, bukan untuk menampilkan data. XML adalah bahasa markup yang mendefinisikan beberapa peraturan untuk menulis dokumen dalam sebuah format yang dapat dibaca oleh baik manusia maupun mesin. Tujuan desain XML berfokus pada kesederhanaan, umum, dan dapat digunakan di dunia internet.

Perbedaan dari JSON dan XML sendiri adalah:
- JSON adalah JavaScript Object Notation, XML adalah Extensible Markup Language
- JSON didasarkan pada JavaScript Language, XML berasal dari SGML
- JSON digunakan sebagai cara untuk merepresentasikan objek, XML adalah bahasa Markup dan menggunakan struktur tag untuk mewakili item data
- JSON tidak mendukung penggunaan namespaces, XML mendukung penggunaan namespaces
- JSON mendukung arrays, XML tidak mendukung arrays
- JSON filenya mudah dibaca dibandingkan dengan XML, XML filenya lebih susah dibaca dan dipahami
- JSON tidak memakai end tags, XML memakai start dan end tags
- JSON kurang aman, XML lebih aman jika dibandingkan dengan JSON
- JSON mendukung comments, XML tidak mendukung comments
- JSON hanya mendukung UTF-8 encoding, XML mendukung banyak variasi encoding


2. Apakah perbedaan antara HTML dan XML?
Berbeda dengan XML, Hypertext Markup Language (HTML) adalah bahasa komputer yang membentuk sebagian besar halaman web dan aplikasi online. Hypertext adalah teks yang digunakan untuk merujuk potongan teks lainnya sedangkan bahasa markup adalah serangkaian penandaan yang memberi tahu server web gaya dan struktur dokumen.

Perbedaan dari HTML dan XML sendiri adalah:
- XML adalah Extensible Markup Language, HTML adalah Hypertext Markup Language
- XML menyediakan sebuah framework atau sebuah kerangka kerja untuk menentukan bahasa markup, HTML hanyalah bahasa markup standar
- XML mengandung informasi struktural, HTML tidak mengandung informasi struktural
- XML memiliki tipe bahasa case Sensitive atau peka terhadap besar dan kecil kalimat, HTML memiliki tipe bahasa case insesitive atau tidak peka terhadap besar dan kecil kalimat
- XML memiliki tujuan mentransfer informasi, HTML memiliki tujuan menyajikan data
- XML tidak boleh memiliki error, HTML yang mengandung error kecil dapat diabaikan
- XML bisa mempertahankan spasi, HTML tidak bisa mempertahankan spasi
- XML harus menggunakan tag penutup, HTML tidak harus menggunakan tag penutup (optional)
- XML nesting atau urutannya harus dilakukan dengan benar, XML nesting atau urutannya tidak terlalu diperhitungkan

sumber:
https://www.geeksforgeeks.org/difference-between-json-and-xml/
https://www.hostinger.com/tutorials/what-is-html
https://dosenit.com/kuliah-it/pemrograman/perbedaan-xml-dan-html